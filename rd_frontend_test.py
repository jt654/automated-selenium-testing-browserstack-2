import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# BrowserStack Username and Password
BS_USERNAME = os.environ['BS_USERNAME']
BS_PASSWORD = os.environ['BS_PASSWORD']

# Reserach Dashboard Username and Password
RD_USERNAME = os.environ['RD_USERNAME']
RD_PASSWORD = os.environ['RD_PASSWORD']

# Environment variable: Server for Research Dashboard URLs
TARGET_SERVER = os.environ.get('TARGET_SERVER')


class RDFunctionalTest(unittest.TestCase):
    # Set Up BrowserStack Username and Password
    def setUp(self):
        self.url = f'https://{BS_USERNAME}:{BS_PASSWORD}@hub-cloud.browserstack.com/wd/hub'

    # Find Server for URLs and Title for Research Dashboard 
    def bs_test(self, desired_cap):
        self.driver = webdriver.Remote(
            command_executor=self.url, desired_capabilities=desired_cap)
        self.driver.get(f"{TARGET_SERVER}")
        if not "Principal Investigator Dashboard" in self.driver.title:
            raise Exception("Unable to load Research Dashboard!")

        # Check that the sign in button exists and click on it
        self.driver.find_element_by_css_selector("body > div > div > main > div > div:nth-child(1) > div:nth-child(3) > a").click()
        
        self.driver.implicitly_wait(20) # seconds

        # Log in to Research Dashboard with Raven Username and Password
        self.driver.current_url.startswith('https://raven.cam.ac.uk/') 
        self.driver.find_element_by_id("userid").send_keys(RD_USERNAME)
        self.driver.find_element_by_id("pwd").send_keys(RD_PASSWORD)
        self.driver.find_element_by_name("submit").click()

        self.driver.implicitly_wait(30) # seconds

        # Check 'Grants' section
        self.driver.current_url.startswith(f"{TARGET_SERVER}grants/")
        self.driver.implicitly_wait(30) # seconds
        self.driver.find_element_by_xpath("//*[@id='app']/div/div[2]/div/div/div/div[2]/ul[1]/a[1]")

        self.driver.implicitly_wait(20) # seconds

        # Check 'Applications' section
        self.driver.get(f"{TARGET_SERVER}application-deadlines/")
        self.driver.implicitly_wait(30) # seconds
        self.driver.find_element_by_xpath("//*[@id='app']/div/div[2]/div/div/div/div[2]/ul[1]/a[2]")

        self.driver.implicitly_wait(20) # seconds

        # Check 'Contracts' section
        self.driver.get(f"{TARGET_SERVER}research-contracts/")
        self.driver.implicitly_wait(30) # seconds
        self.driver.find_element_by_xpath("//*[@id='app']/div/div[2]/div/div/div/div[2]/ul[1]/a[3]")

        self.driver.implicitly_wait(20) # seconds

        # Check 'Contacts' section
        self.driver.get(f"{TARGET_SERVER}contact/")
        self.driver.implicitly_wait(30) # seconds
        self.driver.find_element_by_xpath("//*[@id='app']/div/div[2]/div/div/div/div[2]/ul[1]/a[4]")

    # Check Browsers and Devices
    def test_google_pixel_3(self):
        desired_cap = {
            'browserName': 'Android',
            'device': 'Google Pixel 3',
            'realMobile': 'true',
            'os_version': '9.0',
            'name': 'RD Google Pixel Test'
        }
        self.bs_test(desired_cap)

    def test_samsung_galaxy_s10(self):
        desired_cap = {
            'browserName': 'Android',
            'device': 'Samsung Galaxy S10',
            'realMobile': 'true',
            'os_version': '9.0',
            'name': 'RD Samsung Galaxy Test'
        }
        self.bs_test(desired_cap)

    def test_iphone_xs(self):
        desired_cap = {
            'browserName': 'iPhone',
            'device': 'iPhone XS',
            'realMobile': 'true',
            'os_version': '13',
            'name': 'RD iPhone XS Test'
        }
        self.bs_test(desired_cap)

    def test_samsung_galaxy_s5e(self):
        desired_cap = {
            'browserName': 'Android',
            'device': 'Samsung Galaxy Tab S5e',
            'os_version': '9.0',
            'name': 'RD Samsung Galaxy Tab S5e Test'
        }
        self.bs_test(desired_cap)

    def test_ipad(self):
        desired_cap = {
            'browserName': 'iOS',
            'device': 'iPad 7th',
            'os_version': '13',
            'name': 'RD iPad 7th Test'
        }
        self.bs_test(desired_cap)

    def test_chrome_windows(self):
        desired_cap = {
            'browser': 'Chrome',
            'browser_version': '77',
            'os': 'Windows',
            'os_version': '10',
            'resolution': '1280x800',
            'name': 'RD W10 Chrome Test'
        }   
        self.bs_test(desired_cap)

    def test_firefox_windows(self):
        desired_cap = {
            'browser': 'Firefox',
            'browser_version': '69',
            'os': 'Windows',
            'os_version': '10',
            'resolution': '1280x800',
            'name': 'RD W10 Firefox Test'
        }
        self.bs_test(desired_cap)

    def test_edge_windows(self):
        desired_cap = {
            'browser': 'Edge',
            'browser_version': '18',
            'os': 'Windows',
            'os_version': '10',
            'resolution': '1280x800',
            'name': 'RD W10 Edge Test'
        }
        self.bs_test(desired_cap)

    def test_safari_osx(self):
        desired_cap = {
            'browser': 'Safari',
            'browser_version': '12.1',
            'os': 'OS X',
            'os_version': 'Mojave',
            'resolution': '1280x960',
            'name': 'RD OSX Safari Test'
        }
        self.bs_test(desired_cap)

    def test_chrome_osx(self):
        desired_cap = {
            'browser': 'Chrome',
            'browser_version': '77',
            'os': 'OS X',
            'os_version': 'Mojave',
            'resolution': '1280x960',
            'name': 'RD OSX Chrome Test'
        }
        self.bs_test(desired_cap)

    def test_firefox_osx(self):
        desired_cap = {
            'browser': 'Firefox',
            'browser_version': '69',
            'os': 'OS X',
            'os_version': 'Mojave',
            'resolution': '1280x960',
            'name': 'RD OSX Firefox Test'
        }
        self.bs_test(desired_cap)

    def test_ie_windows(self):
        desired_cap = {
            'browserName': 'IE',
            'browser_version': '11.0',
            'os': 'Windows',
            'os_version': '10',
            'resolution': '1024x768',
            'name': 'RD Windows IE Test'
        }
        self.bs_test(desired_cap)

    def tearDown(self):
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)
        if result.failures:
            self.driver.save_screenshot('%s.png' % str(result.failures[0][0]).split(' ')[0])
        if result.errors:
            self.driver.save_screenshot('%s.png' % str(result.errors[0][0]).split(' ')[0])
        self.driver.quit()

if __name__ == "__main__":
    unittest.main(warnings='ignore')